This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

How to run the application:

### `npm install`

Installs the required libraries and dependancies <br>

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

**Description:**

Jamming is a web application for creating playlists and saving playlists using React, Node, and CSS. The application makes use of Spotify’s API to allow a user to save the created playlist to their account.